package org.tw;

public class Cloning {
    public static void main(String[] args) throws CloneNotSupportedException {
        /**
         * 1. shallow copy
         * 2. deep copy
         * 3. clone
         */
        System.out.println("********Shallow copy*******");
        Abc obj = new Abc();
        obj.i = 5;
        obj.j = 6;
        System.out.println("obj " + obj);
        Abc obj1 = obj;//shallow copy
        System.out.println("obj1 " + obj1);
        //if we update obj1
        obj1.i = 10;
        System.out.println("******updating*******");
        System.out.println("obj " + obj);
        System.out.println("obj1 " + obj1);
//        in shallow copy two references will be there for one object so if we change at one place it will update at both the references
        System.out.println();
        System.out.println("********Deep copy*****");
        Abc obj2 = new Abc();
        obj2.i = obj.i;
        obj2.j = obj.j;
        System.out.println("obj " + obj);
        System.out.println("obj2" + obj2);
        System.out.println("******updating******");
        obj2.i = 25;
        obj2.j = 30;
        System.out.println("obj " + obj);
        System.out.println("obj2" + obj2);

        System.out.println("******clone******");
        Abc obj3 = (Abc) obj.clone();
        System.out.println("obj " + obj);
        System.out.println("obj3 " + obj3);

        System.out.println("********clone updating********");
        obj3.i = 35;
        obj3.j = 45;
        System.out.println("obj " + obj);
        System.out.println("obj3 " + obj3);
    }
}
